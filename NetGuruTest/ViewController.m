//
//  ViewController.m
//  NetGuruTest
//
//  Created by Ilinca on 11/09/2016.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import "ViewController.h"
#import "ArticleCollectionViewCell.h"
#import "DetailsViewController.h"

#define SCREEN_SIZE ([UIScreen mainScreen].bounds.size)
#define PADDING_BETWEEN_CELLS                (10.0)

#define CELL_WIDTH                          (200.0)
#define CELL_HEIGHT                         (250.0)

#define ELEMENTS_NUMBER                      (10)

#define kAnimationDelay 0.08

@interface ViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, NSURLSessionDelegate> {
    CGFloat _previousOffset;
    int _currentPage;
    int _elementsNumber;
    CGPoint _currentFrame;
}

@property (nonatomic, strong) UICollectionView *articlesCollection;
@property (nonatomic, strong) UILabel *errorLabel;
@property (nonatomic, strong) NSArray *responseData;
@property (nonatomic, strong) NSMutableArray *imagesArray;
@property (nonatomic, strong) NSMutableArray *cachedImagesArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self downloadData];
    [self setupLayout];
    self.cachedImagesArray = [NSMutableArray array];
}

-(BOOL)shouldAutorotate {
    return  YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(    NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}
- (void)viewWillLayoutSubviews;
{
    [super viewWillLayoutSubviews];
    self.articlesCollection.frame = self.view.frame;
}

- (void)setupLayout {
    self.navigationController.navigationBar.hidden = YES;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.articlesCollection = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:layout];
    [self.articlesCollection setCollectionViewLayout:layout animated:YES];
    self.articlesCollection.dataSource = self;
    self.articlesCollection.delegate = self;
    [self.articlesCollection registerClass:[ArticleCollectionViewCell class] forCellWithReuseIdentifier:@"articleCell"];
    [self.view addSubview:self.articlesCollection];
    self.errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x, self.view.center.y, 200, 40)];
    self.errorLabel.text = @"Something went wrong. Please restart the app";
    self.errorLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    self.errorLabel.textColor = [UIColor redColor];
    self.errorLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.errorLabel];
    self.errorLabel.hidden = YES;
}

-(void)downloadData {
    NSString *urlData = @"http://gameofthrones.wikia.com/api/v1/Articles/Top?expand=1&category=%E2%80%9CCharacters%E2%80%9D&limit=75";
    
    NSURL *url = [NSURL URLWithString:urlData];

    self.imagesArray = [NSMutableArray array];
    __weak ViewController *weakself = self;
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if (error) {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      weakself.errorLabel.hidden = NO;
                                                  });
                                              }
                                              else {
                                                  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                  NSLog(@"%@", json);
                                                  weakself.responseData = [json objectForKey:@"items"];
                                              
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [weakself.articlesCollection reloadData];
                                                  });
                                              
                                              }
                                          }];
    [dataTask resume];
}


-(void) downloadThumbnailsFromURL:(NSURL *)url {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
    }];
    [downloadTask resume];
}

#pragma mark - UICollectionViewDelegate&Datasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.responseData.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ArticleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"articleCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[ArticleCollectionViewCell alloc] init];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    NSDictionary *cellData = [self.responseData objectAtIndex:indexPath.row];
    cell.titleLabel.text = [cellData objectForKey:@"title"];
    cell.articlePreviewLabel.text = [cellData objectForKey:@"abstract"];
    
    if (self.cachedImagesArray.count < self.responseData.count) {
        NSURL *thumbnailURL = [NSURL URLWithString: [cellData objectForKey:@"thumbnail"]];
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:thumbnailURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.thumbnailImage.image = image;
                    });
                    [self.cachedImagesArray addObject:image];
                }
            }
        }];
        [task resume];
    } else {
        cell.thumbnailImage.image = [self.cachedImagesArray objectAtIndex:indexPath.row];
    }
    
    cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0, 0);
    [UIView animateWithDuration:0.3 delay:0.3 options:0 animations:^{
        cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
            
    }completion:^(BOOL finished){
    }];
        return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DetailsViewController *detailsVC = [[DetailsViewController alloc] init];
    NSDictionary *articleData = [self.responseData objectAtIndex:indexPath.row];
    detailsVC.articleTitleString = [articleData objectForKey:@"title"];
    detailsVC.articleString = [articleData objectForKey:@"abstract"];
    detailsVC.wikiUrlString = [articleData objectForKey:@"url"];
    detailsVC.thumbnail = [self.cachedImagesArray objectAtIndex:indexPath.row];
   [self.navigationController pushViewController:detailsVC animated:NO];
}


#pragma mark - Collection view flow delegate

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,
                            (SCREEN_SIZE.width - CELL_WIDTH)/2,
                            0,
                            (SCREEN_SIZE.width - CELL_WIDTH)/2);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CELL_WIDTH,
                      CELL_HEIGHT);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return PADDING_BETWEEN_CELLS;
}

@end



