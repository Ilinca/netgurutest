//
//  DetailsViewController.h
//  NetGuruTest
//
//  Created by Ilinca Ispas on 13/09/16.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property(nonatomic, strong) NSDictionary *articleData;
@property(nonatomic, strong) NSString *articleTitleString;
@property(nonatomic, strong) NSString *articleString;
@property(nonatomic, strong) NSString *wikiUrlString;
@property(nonatomic, strong) UIImage *thumbnail;

@end
