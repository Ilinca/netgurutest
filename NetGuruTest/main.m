//
//  main.m
//  NetGuruTest
//
//  Created by Ilinca on 11/09/2016.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
