//
//  EpisodeCollectionViewCell.h
//  NetGuruTest
//
//  Created by Ilinca on 11/09/2016.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCollectionViewCell : UICollectionViewCell<NSURLSessionDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *articlePreviewLabel;
@property (nonatomic, strong) UIImageView *thumbnailImage;

@end
