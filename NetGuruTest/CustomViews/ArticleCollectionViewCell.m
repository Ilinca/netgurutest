//
//  EpisodeCollectionViewCell.m
//  NetGuruTest
//
//  Created by Ilinca on 11/09/2016.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import "ArticleCollectionViewCell.h"

@interface ArticleCollectionViewCell()

@property (nonatomic) BOOL isExpanded;

@end

@implementation ArticleCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(0, 0, 200.0, 250.0)];
    [self setupLayout];
    return self;
}

#pragma mark - Layout

-(void)setupLayout
{
    self.isExpanded = NO;
    self.layer.cornerRadius = 4;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 0;
    
    self.thumbnailImage = [[UIImageView alloc] init];
    self.thumbnailImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.thumbnailImage.layer.cornerRadius = 4;
    self.thumbnailImage.layer.masksToBounds = YES;
    self.thumbnailImage.layer.borderWidth = 0;
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18.0];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.articlePreviewLabel = [[UILabel alloc] init];
    self.articlePreviewLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    self.articlePreviewLabel.numberOfLines = 2;
    self.articlePreviewLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.articlePreviewLabel];
    [self addSubview:self.thumbnailImage];
    
    NSDictionary *viewsDict = @{@"thumbnailImage" : self.thumbnailImage, @"titleLabel":self.titleLabel, @"articlePreviewLabel":self.articlePreviewLabel};
    
    //size constraints
    
    NSArray *constraint_image_size_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[thumbnailImage(70)]"
                                                                    options:0
                                                                    metrics:nil
                                                                      views:viewsDict];
    NSArray *constraint_image_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[thumbnailImage(70)]"
                                                                    options:0
                                                                    metrics:nil
                                                                      views:viewsDict];
    NSArray *constraint_title_size_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[titleLabel(80)]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    NSArray *constraint_title_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel(30)]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];

    NSArray *constraint_preview_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[articlePreviewLabel(90)]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    //position constraints
    
    NSArray *constraint_image_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[thumbnailImage]-20-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDict];
    NSArray *constraint_image_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[thumbnailImage]-160-|"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    
    NSArray *constraint_title_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[titleLabel]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    NSArray *constraint_title_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[titleLabel]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    NSArray *constraint_preview_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[articlePreviewLabel]-20-|"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    NSArray *constraint_preview_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[articlePreviewLabel]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    
    
    [self.thumbnailImage addConstraints:constraint_image_size_H];
    [self.thumbnailImage addConstraints:constraint_image_size_V];
    [self addConstraints:constraint_image_pos_H];
    [self addConstraints:constraint_image_pos_V];
    [self.titleLabel addConstraints:constraint_title_size_H];
    [self.titleLabel addConstraints:constraint_title_size_V];
    [self addConstraints:constraint_title_pos_H];
    [self addConstraints:constraint_title_pos_V];
    [self.articlePreviewLabel addConstraints:constraint_preview_size_V];
    [self addConstraints:constraint_preview_pos_H];
    [self addConstraints:constraint_preview_pos_V];
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressureGesture:)];
    longGesture.numberOfTouchesRequired = 1;
    longGesture.minimumPressDuration = 2.0;
    longGesture.delegate = self;
    [self addGestureRecognizer:longGesture];
}

-(void)handleLongPressureGesture:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if (!self.isExpanded) {
            self.articlePreviewLabel.numberOfLines = 10;
        } else {
            self.articlePreviewLabel.numberOfLines = 2;
        }
        self.isExpanded = !self.isExpanded;
    }
}

@end
