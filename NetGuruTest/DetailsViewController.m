//
//  DetailsViewController.m
//  NetGuruTest
//
//  Created by Ilinca Ispas on 13/09/16.
//  Copyright © 2016 ilinca. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *articleLabel;
@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UIButton *wikiButton;
@property (nonatomic, strong) NSArray *portraitConstraintsArray;
@property (nonatomic, strong) NSArray *landscapeConstraintsArray;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(    NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}
- (void)viewWillLayoutSubviews;
{
    [super viewWillLayoutSubviews];
}


-(void) setupLayout {
    self.titleLabel = [[UILabel alloc] init];
    self.articleLabel = [[UILabel alloc] init];
    self.thumbnailImageView = [[UIImageView alloc] init];
    self.wikiButton = [[UIButton alloc] init];
    
    self.thumbnailImageView.image = self.thumbnail;
    self.thumbnailImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.thumbnailImageView.layer.cornerRadius = 4;
    self.thumbnailImageView.layer.masksToBounds = YES;
    self.thumbnailImageView.layer.borderWidth = 0;
    
    self.wikiButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.wikiButton.layer.cornerRadius = 4;
    self.wikiButton.layer.masksToBounds = YES;
    self.wikiButton.layer.borderWidth = 1;
    self.wikiButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [self.wikiButton setTitle:@"WIKI PAGE" forState:UIControlStateNormal];
    [self.wikiButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.wikiButton addTarget:self
                        action:@selector(wikiButtonTapped:)
              forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24.0];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = self.articleTitleString;
    
    self.articleLabel = [[UILabel alloc] init];
    self.articleLabel.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    self.articleLabel.adjustsFontSizeToFitWidth = YES;
    self.articleLabel.numberOfLines = 8;
    self.articleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.articleLabel.textAlignment = NSTextAlignmentCenter;
    self.articleLabel.text = self.articleString;
    
    [self setupConstraints];
}

-(void) setupConstraints {
    [self.view addSubview:self.thumbnailImageView];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.articleLabel];
    [self.view addSubview:self.wikiButton];
    
    NSDictionary *viewsDict = @{@"thumbnailImageView":self.thumbnailImageView, @"wikiButton":self.wikiButton, @"titleLabel":self.titleLabel, @"articleLabel":self.articleLabel};
    
    NSArray *constraint_thumbnail_size_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[thumbnailImageView(120)]"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:viewsDict];
    NSArray *constraint_thumbnail_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[thumbnailImageView(120)]"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:viewsDict];
    
    [self.thumbnailImageView addConstraints:constraint_thumbnail_size_H];
    [self.thumbnailImageView addConstraints:constraint_thumbnail_size_V];
    
    NSArray *constraint_thumbnail_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[thumbnailImageView]"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:viewsDict];
    [self.view addConstraints:constraint_thumbnail_pos_V];
    
    NSArray *constraint_button_size_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[wikiButton(120)]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDict];
    NSArray *constraint_button_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[wikiButton(40)]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDict];
    [self.wikiButton addConstraints:constraint_button_size_H];
    [self.wikiButton addConstraints:constraint_button_size_V];
    
    NSArray *constraint_button_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[thumbnailImageView]-30-[wikiButton]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    NSArray *constraint_button_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[wikiButton]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    [self.view addConstraints:constraint_button_pos_H];
    [self.view addConstraints:constraint_button_pos_V];
    
    
    NSArray *constraint_title_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel(40)]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    
    [self.titleLabel addConstraints:constraint_title_size_V];
    
    
    NSArray *constraint_title_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[thumbnailImageView]-30-[titleLabel]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    NSArray *constraint_title_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-140-[titleLabel]"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDict];
    [self.view addConstraints:constraint_title_pos_H];
    [self.view addConstraints:constraint_title_pos_V];
    
    NSArray *constraint_article_size_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[articleLabel(150)]"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDict];
    
    [self.articleLabel addConstraints:constraint_article_size_V];
    
    
    NSArray *constraint_article_pos_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[articleLabel]-20-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDict];
    NSArray *constraint_artile_pos_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-180-[articleLabel]"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewsDict];
    [self.view addConstraints:constraint_article_pos_H];
    [self.view addConstraints:constraint_artile_pos_V];
 
}

-(IBAction)wikiButtonTapped:(UIButton *)button {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://gameofthrones.wikia.com/%@" , self.wikiUrlString]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
